import express from "express";
import cors from "cors";
import bodyParser from "body-parser";
import { GoogleGenerativeAI } from "@google/generative-ai";
import dotenv from "dotenv";

dotenv.config();

const app = express();
const port = process.env.PORT || 3000;

// Allow requests from the origin where your frontend is deployed
const allowedOrigins = [
  "https://ai-task-creator.vercel.app",
  // Add more origins if needed
];

const corsOptions = {
  origin: (origin, callback) => {
    if (allowedOrigins.includes(origin) || !origin) {
      callback(null, true);
    } else {
      callback(new Error("Not allowed by CORS"));
    }
  },
};

app.use(cors(corsOptions));
app.use(bodyParser.json());

const genAI = new GoogleGenerativeAI(process.env.GOOGLE_GEN_AI_HACKATHON_KEY);

app.post("/gemini", async (req, res) => {
  // Your existing code
});

app.get("/gemini-history", (req, res) => {
  // Your existing code
});

app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
